import { storiesOf } from "@storybook/vue";
import { withInfo } from "storybook-addon-vue-info";

import Button from "./Button/Button.vue";

import ButtonPlayground from "../playground/button-playground.vue";

storiesOf("Components/Button", module)
	.addParameters({ options: { showPanel: false } })
	.addDecorator(withInfo())
	.add(
		"propreties",
		() => ({
			components: { Button },
			template: "<Button>Button</Button>"
		}),
		{ info: { docsInPanel: false } }
	)
	.add(
		"playground",
		() => ({
			components: { ButtonPlayground },
			template: "<ButtonPlayground />"
		}),
		{ info: false }
	);
