import test from "ava";
import { mount } from "@vue/test-utils";

import Button from "./Button.vue";

test("Button/assign color prop", t => {
	const wrapper = mount(Button, {
		propsData: {
			color: "white"
		}
	});
	t.is(wrapper.props().color, "white");
	t.deepEqual(wrapper.classes(), ["button", "white", "normal"]);
});

test("Button/default color prop", t => {
	const wrapper = mount(Button);
	t.is(wrapper.props().color, "black");
	t.deepEqual(wrapper.classes(), ["button", "black", "normal"]);
});

test("Button/overload class name", t => {
	const wrapper = mount(Button);
	wrapper.setProps({ class: "my-custom-class-name" });
	t.deepEqual(wrapper.classes(), [
		"button",
		"black",
		"normal",
		"my-custom-class-name"
	]);
});
