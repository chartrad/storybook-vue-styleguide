export default {
	compileEnhancements: false,
	sources: ["stories"],
	require: ["./test/_setup"]
};
