const path = require("path");

module.exports = {
	module: {
		rules: [
			{
				test: /\.(s)css$/,
				include: path.resolve(__dirname, "../"),
				use: [
					{
						loader: "vue-style-loader"
					},
					{
						loader: "css-loader",
						options: {
							modules: true,
							localIdentName: "[name]__[local]--[hash:base64:5]"
						}
					},
					{
						loader: "sass-loader"
					}
				]
			},
			{
				test: /\.vue$/,
				enforce: "post",
				use: [{ loader: "storybook-addon-vue-info/loader" }]
			}
		]
	}
};
