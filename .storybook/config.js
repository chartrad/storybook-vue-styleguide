import { addParameters, configure } from "@storybook/vue";
import { create } from "@storybook/theming";

const req = require.context("../stories", true, /.stories.js$/);

function loadStories() {
	req.keys().forEach(filename => req(filename));
}

addParameters();
configure(loadStories, module);
